# Open Dictionary Library

[![pipeline status](https://gitlab.com/prvInSpace/open-dictionary-library/badges/master/pipeline.svg)](https://gitlab.com/prvInSpace/open-dictionary-library/-/commits/master)
[![coverage report](https://gitlab.com/prvInSpace/open-dictionary-library/badges/master/coverage.svg)](https://gitlab.com/prvInSpace/open-dictionary-library/-/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-blue.svg)](https://opensource.org/licenses/MIT)
[![Discord](https://img.shields.io/discord/718008955040956456.svg?label=&logo=discord&logoColor=ffffff&color=7389D8&labelColor=6A7EC2)](https://discord.gg/UzaFmfV)

The Open Dictionary library is a flexible foundation on which dictionaries for different languages can be built and interact.
It serves as the foundation for the Open Source Dictionary project.

## Community

Due to the history of the project, most of the developer community is located in the projects [Discord server](https://discord.gg/UzaFmfV). If you got any questions, suggestions, you need any help, or if you just want to pop by to have a cup of coffee or tea, then feel free to join! You are more than welcome to pop by!

The main website for the project is [opensourcedict.org](https://opensourcedict.org).
Here you'll find an interface that allows you to access the different dictionaries, download them as JSON files, and learn more about the project.

## Importing the Library using Gradle

The latest version of the library is hosted through the package repository here on Gitlab.
That means that you are able to import the library using several build tools.

In the list of repositories in your build.gradle file add the following repository:
```
repositories {
    maven { url "https://gitlab.com/api/v4/projects/24450764/packages/maven" }
}
```
And then in the list of dependencies add a reference to the library like this:
```
dependencies {
    implementation 'cymru.prv:open-dictionary:[1.2.0,1.3.0)'
}
```

## Implementations

Here is a list of dictionaries that use the Open Dictionary library:

| Name                            | Link                                                                         |
|---------------------------------|------------------------------------------------------------------------------|
| Open Breton Dictionary          | [Gitlab Repo](https://gitlab.com/prvInSpace/open-breton-dictionary)          |
| Open Cornish Dictionary         | [Gitlab Repo](https://gitlab.com/prvInSpace/open-cornish-dictionary)         |
| Open French Language Pack       | [Gitlab Repo](https://gitlab.com/prvInSpace/open-french-dictionary)          |
| Open Irish Dictionary           | [Gitlab Repo](https://gitlab.com/prvInSpace/open-irish-dictionary)           |
| Open Nynorsk Language Pack      | [Gitlab Repo](https://gitlab.com/prvInSpace/open-nynorsk-dictionary)         |
| Open Scottish Gaelic Dictionary | [Gitlab Repo](https://gitlab.com/prvInSpace/open-scottish-gaelic-dictionary) |
| Open Spanish Dictionary         | [Gitlab Repo](https://gitlab.com/prvInSpace/open-spanish-dictionary)         |
| Open Welsh Dictionary           | [Gitlab Repo](https://gitlab.com/prvInSpace/open-welsh-dictionary)           |

## Special Thanks and Acknowledgements

I want to extend a special thanks to everyone who has contributed to the project since its inception.
Since the original repository has been split into several repositories I want to list the contributors to that
repository here in no particular order:

* Seán Breathnach
* Gwlanbzh
* Zander Urq.
* Gwenn M
* Issac Richards
* David Kong

## Maintainers

* Preben Vangberg &lt;prv21fgt@bangor.ac.uk&gt;
