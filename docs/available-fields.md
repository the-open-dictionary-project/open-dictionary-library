# Available JSON fields

## Basic information

| Field | Type | Required | Usage |
|-------|------|----------|-------|
| normalForm | String | Yes | The form listed here is the dictionary lookup form of the word, or the standard version. |
| id | Int | No | Used to uniquely indentify a specific word. Has to be unqiue. |
| notes | String | No | Used to add additional information about a word. |
| etymology | String | No | Used to add information about the etymology of a word. |

## Reference list fields

Reference lists are a bit special in that they are lists that can contain both IDs and string literals.
They can be defined like this:
```json
"field1": [ 1, "word", 2 ]
"field2": "word"
"field3": 1
```

| Field | Usage |
|-------|-------|
| related | Used to list related words |
| synonyms | Used to add synonyms of a word |
| antonyms | Used to add antonyms of a word |

### Versions

The `versions` field is used to list versions of a word, be that different spellings, dialectal forms, and so forth.
It is a normal reference list field, however, a special feature for `versions`is that string literals that get added to this field will be added as a redirect to the word.

## Translations

Translations are added to the word by using the `translations` key word.
This can be a single string or a list of strings.
It has some formatting rules though to help with matching translations in other languages:

For a standard set of translations this would suffice:
```json
"translations": "my translation"
```
or
```json
"translations": [ "first translation", "second translation"]
```

However, there exist circumstances where the English language is ambiguous.
In cases like these, specifications can be added to this library and used in the different dictionaries.
For example, the word `to know` is in many languages split into knowing something and knowing someone.
Hence, in this library, both of those two forms have been added.
If you want the first form you can write `to know:1`, the second form `to know:2`, or if both are correct you can write `to know:*`.

Note that there isn't a good resource for looking up these terms yet, so the best way to figure out these is just adding the word and if it prints a warning then lookup the word in this library.

You can also use the so-called "parenthesis notation" where the specification is added to the end of the word.
This will be treated as `:*` though.

## Inflections

## Conjugations

## Mutations

## Additional fields

Additional fields might be added by different languages and for different word types depending on need.
