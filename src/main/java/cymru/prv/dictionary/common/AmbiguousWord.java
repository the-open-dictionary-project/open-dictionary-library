package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Represents a collection of ambiguous versions of a word
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class AmbiguousWord implements JsonSerializable {

    private final String englishWord;

    private final Map<String, AmbiguousWordVersion> versions = new HashMap<>();
    private final Map<WordType, AmbiguousWordVersion> defaultTypes = new HashMap<>();

    AmbiguousWord(JSONObject obj) {
        englishWord = obj.getString("word");
        JSONObject listOfVersions = obj.getJSONObject("versions");
        for (String version : listOfVersions.keySet()) {
            Object o = listOfVersions.get(version);
            AmbiguousWordVersion wordVersion;
            if (o instanceof String) {
                wordVersion = new AmbiguousWordVersion(englishWord, (String) o);
            } else {
                JSONObject versionObj = (JSONObject) o;
                wordVersion = new AmbiguousWordVersion(englishWord, versionObj.getString("text"));
                if (versionObj.has("default")) {
                    WordType type = Enum.valueOf(WordType.class, versionObj.getString("default"));
                    defaultTypes.put(type, wordVersion);
                }
            }
            versions.put(version, wordVersion);
        }
    }


    /**
     * Returns all the the translation units.
     *
     * @return a list of all the translation units.
     */
    List<AmbiguousWordVersion> getTranslationUnits() {
        return new LinkedList<>(versions.values());
    }

    
    /**
     * Fetches all the translation units that are applicable for the given
     * word.
     * <p>
     * The format of the word can be formatted in 3 ways:
     * </p><p>
     * 1: The word without any version information: "word".
     * If the word type does not have a default it will print a warning.
     * </p><p>
     * 2: The word with version number information: "word:1,2".
     * The version numbers come after a colon and is separated with
     * a comma.
     * </p><p>
     * 3: The word with a start instead of numbers: "word:*"
     * This means that all versions apply.
     * </p>
     *
     * @param text the translation string (word)
     * @param type the type
     * @return a list of translation units that matches the text and the type
     */
    List<AmbiguousWordVersion> getTranslationUnits(String text, WordType type) {
        // If it does not contain information about version
        if (!text.contains(":")) {
            // Test for default type
            if (defaultTypes.containsKey(type)) {
                return List.of(defaultTypes.get(type));
            }
            System.err.println("Warning: Word " +
                    text +
                    " contain ambiguous versions. Consider specifying which version is preferred.");
            return new LinkedList<>(versions.values());
        }

        // Get version numbering information from the string
        String versionInformation = text.split(":")[1];

        // Check for star
        if (versionInformation.equals("*"))
            return new LinkedList<>(versions.values());

        // Otherwise add the numbers
        String[] versionNumbers = versionInformation.split(",");
        List<AmbiguousWordVersion> units = new LinkedList<>();
        for (String versionNumber : versionNumbers) {
            if (!versions.containsKey(versionNumber.trim())) {
                System.err.println("Warning: Unknown version " + versionNumber + " for word " + englishWord);
                continue;
            }
            units.add(versions.get(versionNumber));
        }
        return units;
    }


    /**
     * Returns the English word
     *
     * @return the English word
     */
    String getEnglishWord() {
        return englishWord;
    }


    /**
     * Exports the object to a JSONObject
     *
     * @return the object as a JSONObject
     */
    @Override
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put("word", englishWord);
        JSONObject versionsArray = new JSONObject();
        for(String key : versions.keySet())
            versionsArray.put(key, versions.get(key).getText());
        obj.put("versions", versionsArray);
        return obj;
    }
}
