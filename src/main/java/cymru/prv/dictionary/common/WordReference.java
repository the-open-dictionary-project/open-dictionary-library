package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;

/**
 * Used for words to reference themselves as opposed to
 * IDWordReference which is used to reference other words
 *
 * @author Preben Vangberg
 * @since 1.2.0
 */
public class WordReference implements JsonSerializable {

    private final Word word;

    WordReference(Word word){
       this.word = word;
    }

    @Override
    public JSONObject toJson() {
        var obj = new JSONObject();
        obj.put("value", word.getNormalForm());
        if(word.isInDictionary())
            obj.put("lang", word.getDictionary().getLanguageCode());
        if(word.getId() >= 0)
            obj.put("id", word.getId());
        return obj;
    }
}
