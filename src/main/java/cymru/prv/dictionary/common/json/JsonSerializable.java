package cymru.prv.dictionary.common.json;

import org.json.JSONObject;


/**
 * Makes the object serializable to JSON
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public interface JsonSerializable {
    JSONObject toJson();
}
