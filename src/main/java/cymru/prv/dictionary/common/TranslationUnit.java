package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;


/**
 * Represents a collection of translations
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class TranslationUnit implements JsonSerializable {

    protected final String englishWord;
    private final Map<String, List<WordReference>> translations = new HashMap<>();

    TranslationUnit(String englishWord){
        this.englishWord = englishWord;
    }

    void addWord(Word word) {
        translations.putIfAbsent(word.getDictionary().getLanguageCode(), new LinkedList<>());
        translations.get(word.getDictionary().getLanguageCode()).add(new WordReference(word));
    }

    JSONObject toJson(String originLangauge) {
        JSONObject obj = new JSONObject();
        obj.put("en", new JSONArray().put(new LiteralWordReference(englishWord)
                .toJson()
                .put("lang", "en")));
        for(String langCode : translations.keySet())
            if(!originLangauge.equals(langCode))
                obj.put(langCode, Json.toJsonArray(translations.get(langCode)));
        return obj;
    }

    @Override
    public JSONObject toJson() {
        return toJson("");
    }
}
