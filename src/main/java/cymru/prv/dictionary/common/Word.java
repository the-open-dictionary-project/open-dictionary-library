package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Supplier;

/**
 * Represents a single word.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class Word implements JsonSerializable {

    // Field name constants
    protected static final String NORMALFORM = "normalForm";
    private static final String ID = "id";
    private static final String ETYMOLOGY = "etymology";
    private static final String EXAMPLES = "examples";
    private static final String NOTES = "notes";
    private static final String MUTATIONS = "mutations";
    private static final String CONJUGATIONS = "conjugations";
    private static final String INFLECTIONS = "inflections";
    private static final String VERSIONS = "versions";
    private static final String RELATED = "related";
    private static final String SYNONYMS = "synonyms";
    private static final String ANTONYMS = "antonyms";
    private static final String TROPONYMS = "troponyms";
    private static final String TRANSLATIONS = "translations";

    // Fields
    private final long id;

    private final String normalForm;
    private final WordType type;
    private final String etymology;
    private final String notes;
    private final List<UsageExample> usageExamples;
    private boolean confirmed = true;

    // Lists containing references and strings
    private final List<JsonSerializable> versions;
    private final List<JsonSerializable> related;
    private final List<JsonSerializable> synonyms;
    private final List<JsonSerializable> antonyms;
    private final List<JsonSerializable> troponyms;

    // Translations
    private final List<String> translations;

    private Dictionary dictionary;

    /**
     * Creates an instance of the word.
     * The field normalForm is required and can not be empty.
     *
     * @throws IllegalArgumentException if normalForm is empty or null
     * @param obj The data for the word
     * @param type The word type of the word
     */
    public Word(JSONObject obj, WordType type){
        this.type = type;
        normalForm = obj.optString(NORMALFORM, "");
        id = obj.optLong(ID, -1);
        if(normalForm.trim().isBlank())
            throw new IllegalArgumentException("Normal form cannot be empty.\n" + obj.toString());

        etymology = obj.optString(ETYMOLOGY, null);
        notes = obj.optString(NOTES, null);

        versions  = getReferencesFromArrayIfExist(obj, VERSIONS);
        related   = getReferencesFromArrayIfExist(obj, RELATED);
        synonyms  = getReferencesFromArrayIfExist(obj, SYNONYMS);
        antonyms  = getReferencesFromArrayIfExist(obj, ANTONYMS);
        troponyms = getReferencesFromArrayIfExist(obj, TROPONYMS);

        if(obj.has(EXAMPLES))
            usageExamples = Json.getObjectList(obj.getJSONArray(EXAMPLES), UsageExample::new);
        else
            usageExamples = null;

        translations = Json.getStringList(obj, TRANSLATIONS);
    }

    Word addToDictionary(Dictionary dictionary){
        this.dictionary = dictionary;
        return this;
    }


    @Override
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();
        obj.put(NORMALFORM, normalForm);
        obj.put("type", type);
        if(id >= 0)
            obj.put("id", id);

        obj.put("confirmed", confirmed);
        if(isInDictionary())
            obj.put("lang", dictionary.getLanguageCode());


        obj.put(TRANSLATIONS, getTranslationsArray());

        addIfExist(obj, ETYMOLOGY, etymology);
        addIfExist(obj, NOTES, notes);

        addIfExist(obj, MUTATIONS, this::getMutations);
        addIfExist(obj, CONJUGATIONS, this::getConjugations);
        addIfExist(obj, INFLECTIONS, this::getInflections);

        if(versions.size() > 0)
            obj.put(VERSIONS, Json.toJsonArray(versions));
        if(related.size() > 0)
            obj.put(RELATED, Json.toJsonArray(related));
        if(synonyms.size() > 0)
            obj.put(SYNONYMS, Json.toJsonArray(synonyms));
        if(antonyms.size() > 0)
            obj.put(ANTONYMS, Json.toJsonArray(antonyms));
        if(troponyms.size() > 0)
            obj.put(TROPONYMS, Json.toJsonArray(troponyms));

        if(usageExamples != null)
            obj.put(EXAMPLES, Json.toJsonArray(usageExamples));

        return obj;
    }


    private JSONArray getTranslationsArray(){
        JSONArray array = new JSONArray();
        if(isInDictionary()){
            for(var unit : dictionary.getTranslationUnits(this))
                array.put(unit.toJson(dictionary.getLanguageCode()));
        }
        else {
            // This is not very elegant, or efficient, but it works
            var list = new DictionaryList();
            for(var trans : translations)
                for(var unit : list.getTranslationUnits(trans, type))
                    array.put(unit.toJson());
        }
        return array;
    }

    /**
     * Splits the JSON array and puts the normal strings
     * in the references list and the id references in
     * the indexes list
     *
     * @param obj the object in which the array is contained
     * @param key the key of the array
     */
    private List<JsonSerializable> getReferencesFromArrayIfExist(JSONObject obj, String key){
        if(!obj.has(key))
            return Collections.emptyList();

        List<JsonSerializable> references = new LinkedList<>();
        Object object = obj.get(key);
        if(object instanceof JSONArray) {
            JSONArray array = obj.getJSONArray(key);
            for (int i = 0; i < array.length(); ++i) {
                Object value = array.get(i);
                if (value instanceof String)
                    references.add(new LiteralWordReference((String) value));
                else {
                    long index = array.getLong(i);
                    references.add(new IDWordReference(this, index));
                }
            }
        }
        else if(object instanceof String)
            references.add(new LiteralWordReference((String) object));
        else {
            long index = obj.getLong(key);
            references.add(new IDWordReference(this, index));
        }

        return references;
    }


    /**
     * Adds the result from the supplier if
     * the result is not null
     *
     * @param obj The object to add the value to
     * @param key The key of the value
     * @param supplier The supplier of the value
     */
    private static <T> void addIfExist(JSONObject obj, String key, Supplier<T> supplier){
        T value = supplier.get();
        if(value != null)
            obj.put(key, value);
    }

    /**
     * If the value is not null it will be
     * added to the object. Otherwise
     * it will not.
     *
     * @param obj The object to add the value to
     * @param key The key of the value
     * @param value The value to add
     */
    protected static <T> void addIfExist(JSONObject obj, String key, T value){
        if(value != null)
            obj.put(key, value);
    }

    /**
     * If value is not null, the value will be added to the object
     * with the key provided. Otherwise it will add whatever it
     * gets from the supplier function.
     *
     * @param obj The object to add the value to
     * @param key The key of the value
     * @param value The value to add
     * @param supplier The default backup value
     */
    protected static <T> void addOrDefault(JSONObject obj, String key, T value, Supplier<T> supplier){
        if(value != null)
            obj.put(key, value);
        else
            obj.put(key, supplier.get());
    }


    /**
     * Fetches the mutations for the word. If a word
     * has mutations it should extend this function.
     * If the function returns null, the field mutations
     * will not be exported
     *
     * @return the inflections of the word
     */
    protected JSONObject getMutations(){
        return null;
    }


    /**
     * Fetches the conjugations for the word. If a word
     * has conjugations it should extend this function.
     * If the function returns null, the field conjugations
     * will not be exported
     *
     * @return the conjugations of the word
     */
    protected JSONObject getConjugations(){
        return null;
    }

    /**
     * Fetches the inflections for the word. If a word
     * has inflections it should extend this function.
     * If the function returns null, the field inflections
     * will not be exported
     *
     * @return the inflections of the word
     */
    protected JSONObject getInflections() { return null; }


    /**
     * Returns the normal form of the word
     *
     * @return the normal form of the wod
     */
    public String getNormalForm() {
        return normalForm;
    }


    /**
     * Returns the id for the word
     *
     * @return the id for the word
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the confirmed status to the
     * given boolean.
     *
     * @return The word itself
     */
    Word setConfirmed(boolean confirmed){
        this.confirmed = confirmed;
        return this;
    }


    /**
     * Returns the word type of the word
     * @return the word type of the word
     */
    public WordType getType() {
        return type;
    }

    /**
     * Returns all versions of the word as a list
     * of strings. This includes the normal form
     * of the word.
     *
     * @return a list of all the forms of the word
     */
    public List<String> getVersions(){
        List<String> words = new LinkedList<>();
        words.add(normalForm);
        words.addAll(getRedirects());
        return words;
    }


    List<String> getRedirects(){
        List<String> list = new LinkedList<>();
        for(JsonSerializable obj : versions)
            if(obj instanceof LiteralWordReference)
                list.add(obj.toJson().getString("value"));

        return list;
    }

    public boolean isInDictionary() {
        return dictionary != null;
    }

    public Dictionary getDictionary() {
        return dictionary;
    }

    List<String> getTranslationStrings() {
        return translations;
    }

    void verifyIDReferences() {
        var lists = List.of(
                versions,
                related,
                synonyms,
                antonyms,
                troponyms
        );

        // Attempt to resolve target for all ID word references
        for(var list : lists)
            for(var ref : list)
                if(ref instanceof IDWordReference)
                    ((IDWordReference) ref).resolveTarget();
    }
}
