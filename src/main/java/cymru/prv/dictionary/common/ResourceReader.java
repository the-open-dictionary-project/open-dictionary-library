package cymru.prv.dictionary.common;

import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.IOException;
import java.io.InputStream;
import java.lang.invoke.MethodHandles;
import java.util.LinkedList;
import java.util.List;

/**
 * A helper class to aid with loading
 * JSON resource files from the classpath
 */
final class ResourceReader {

    private ResourceReader(){}

    /**
     * Creates a list of input-streams for every
     * files in the folder in the classpath.
     *
     * @param folder the folder in the classpath to parse
     * @return a list of input-streams
     * @throws IOException Throws an exception if the method is unable list the content of the folder
     */
    static List<InputStream> getFilesInDirectory(String folder) throws IOException {
        ClassLoader classLoader = MethodHandles.lookup().getClass().getClassLoader();
        PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver(classLoader);
        Resource[] resources = resolver.getResources("classpath:" + folder  + "*.json");
        List<InputStream> streams = new LinkedList<>();
        for(Resource resource : resources)
            streams.add(resource.getInputStream());

        return streams;
    }

}
