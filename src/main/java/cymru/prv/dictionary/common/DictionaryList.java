package cymru.prv.dictionary.common;

import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * <p>
 * Represents a list of dictionaries
 * </p>
 * <p>
 * Will automatically load any resources from "/res/common/ambiguities
 * </p>
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class DictionaryList {

    private final Map<String, Dictionary> dictionaries = new HashMap<>();

    // English translations
    private final Map<String, AmbiguousWord> ambiguousTranslations = new HashMap<>();
    private final Map<String, Map<WordType, TranslationUnit>> englishTranslations = new HashMap<>();

    private final Map<Word, Set<TranslationUnit>> translations = new HashMap<>();

    public DictionaryList(){
        String path = "/res/common/ambiguities/";
        try {
            List<InputStream> files = ResourceReader.getFilesInDirectory(path);
            for(InputStream file : files){
                JSONArray array = new JSONArray(new String(file.readAllBytes()));
                for(int i = 0; i < array.length(); ++i){
                    AmbiguousWord unit = new AmbiguousWord(array.getJSONObject(i));
                    ambiguousTranslations.put(unit.getEnglishWord(), unit);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    Collection<? extends TranslationUnit> getTranslationUnits(Word word){
        return translations.getOrDefault(word, new HashSet<>());
    }

    Collection<? extends TranslationUnit> getTranslationUnits(String englishWord, WordType type) {
        if(englishWord.contains("("))
            System.err.println("Warning: '" + englishWord + "': Bracket notation is deprecated. Please use versions instead.");

        String firstPart = englishWord.split(":")[0];
        if(ambiguousTranslations.containsKey(firstPart))
            return ambiguousTranslations.get(firstPart).getTranslationUnits(englishWord, type);
        else if(englishWord.contains(":")) {
            System.err.println("Warning: '" + englishWord + "': No ambiguous unit for this word");
            englishWord = firstPart;
        }

        // If it is in the list of translations
        // return the translation unit
        englishTranslations.putIfAbsent(englishWord, new HashMap<>());
        if(englishTranslations.get(englishWord).containsKey(type))
            return List.of(englishTranslations.get(englishWord).get(type));

        // Create a new translation unit
        TranslationUnit unit = new TranslationUnit(englishWord);
        englishTranslations.get(englishWord).put(type, unit);
        return List.of(unit);
    }


    void addWord(Word word){
        translations.put(word, new HashSet<>());
        for(String trans : word.getTranslationStrings()) {
            var units = getTranslationUnits(trans, word.getType());
            translations.get(word).addAll(units);
            for(var unit : units){
                unit.addWord(word);
            }
        }
    }


    /**
     * Fetches translation units for the english term
     *
     * @param englishWord the term to search for
     * @return a list of translation units
     */
    public List<TranslationUnit> getTranslationsByEnglishWord(String englishWord){
        List<TranslationUnit> units = new LinkedList<>();
        if(ambiguousTranslations.containsKey(englishWord))
            units.addAll(ambiguousTranslations.get(englishWord).getTranslationUnits());
        else if(englishTranslations.containsKey(englishWord)) {
            units.addAll(new LinkedList<>(englishTranslations.get(englishWord).values()));
        }
        return units;
    }

    void addDictionary(Dictionary dictionary){
        dictionaries.put(dictionary.getLanguageCode(), dictionary);
    }


    /**
     * Returns the number of ambiguous terms in the
     * dictionary.
     *
     * @return the number of ambiguous terms
     */
    public long getNumberOfAmbiguousWords(){
        return ambiguousTranslations.size();
    }


    /**
     * Returns a bool indicating whether the dictionary
     * with the code is present in the dictionary list.
     *
     * @param lang the dictionary code
     * @return a bool indicating if the dictionary is present
     */
    public boolean hasDictionary(String lang){
        return dictionaries.containsKey(lang);
    }


    /**
     * Fetches the dictionary with the given language
     * code if it is present.
     *
     * @param lang the dictionary code
     * @return the dictionary with the given code
     */
    public Dictionary getDictionary(String lang){
        return dictionaries.get(lang);
    }


    /**
     * Fetches the ambiguous term for the given word
     *
     * @param englishTranslation the word to search for
     * @return the ambiguous term if it exist or null
     */
    public AmbiguousWord getAmbiguousWord(String englishTranslation){
        return ambiguousTranslations.getOrDefault(englishTranslation, null);
    }


    /**
     * Returns the map containing the dictionaries in the dictionary list
     * @return A copy of the internal dictionary map
     */
    public Map<String, Dictionary> getDictionaries() {
        return new HashMap<>(dictionaries);
    }
}
