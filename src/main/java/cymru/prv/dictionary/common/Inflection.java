package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.Json;
import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.*;
import java.util.function.Supplier;


/**
 * <p>
 * Represents a inflection
 * </p>
 * <p>
 * The class have several overridable methods which
 * are used to generate default inflections.
 * These can be overridden with named fields in the
 * JSON object that are provided to the constructor.
 * If the methods return null the object will not
 * export the field unless it was given a override
 * through the constructor.
 * </p>
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class Inflection implements JsonSerializable {

    private static final String SINGULAR_FIRST = "singFirst";
    private static final String SINGULAR_SECOND = "singSecond";
    private static final String SINGULAR_THIRD = "singThird";
    private static final String SINGULAR_THIRD_MASCULINE = "singThirdMasc";
    private static final String SINGULAR_THIRD_FEMININE = "singThirdFem";
    private static final String PLURAL_FIRST = "plurFirst";
    private static final String PLURAL_SECOND = "plurSecond";
    private static final String PLURAL_THIRD = "plurThird";
    private static final String IMPERSONAL = "impersonal";

    private final boolean hasInflection;

    private final Map<String, List<String>> persons = new HashMap<>();


    /**
     * Creates a new inflection with based on the data
     * in obj.
     *
     * @param obj the data
     */
    public Inflection(JSONObject obj){

        hasInflection = obj.optBoolean("has", true);
        if(!hasInflection)
            return;

        // Singulars
        getOverrides(obj, SINGULAR_FIRST);
        getOverrides(obj, SINGULAR_SECOND);
        getOverrides(obj, SINGULAR_THIRD);
        getOverrides(obj, SINGULAR_THIRD_MASCULINE);
        getOverrides(obj, SINGULAR_THIRD_FEMININE);

        // Plurals
        getOverrides(obj, PLURAL_FIRST);
        getOverrides(obj, PLURAL_SECOND);
        getOverrides(obj, PLURAL_THIRD);
        
        // Other
        getOverrides(obj, IMPERSONAL);
    }


    /**
     * Exports all the inflections in the object.
     * It will export all the fields that are not
     * null.
     *
     * @return the inflections as a JSONObject
     */
    @Override
    public JSONObject toJson() {
        JSONObject obj = new JSONObject();

        getWordList(obj, SINGULAR_FIRST, this::getDefaultSingularFirst);
        getWordList(obj, SINGULAR_SECOND, this::getDefaultSingularSecond);
        getWordList(obj, SINGULAR_THIRD, this::getDefaultSingularThird);
        getWordList(obj, SINGULAR_THIRD_MASCULINE, this::getDefaultSingularThirdMasculine);
        getWordList(obj, SINGULAR_THIRD_FEMININE, this::getDefaultSingularThirdFeminine);

        getWordList(obj, PLURAL_FIRST, this::getDefaultPluralFirst);
        getWordList(obj, PLURAL_SECOND, this::getDefaultPluralSecond);
        getWordList(obj, PLURAL_THIRD, this::getDefaultPluralThird);

        getWordList(obj, IMPERSONAL, this::getDefaultImpersonal);
        
        return obj;
    }

    private void getWordList(JSONObject obj, String key, Supplier<List<String>> getDefault){
        if(persons.containsKey(key))
            obj.put(key, new JSONArray(persons.get(key)));
        else {
            List<String> words = getDefault.get();
            if(words != null)
                obj.put(key, new JSONArray(words));
        }
    }

    private void getOverrides(JSONObject obj, String key){
        if (obj.has(key))
            persons.put(key, Json.getStringList(obj, key));
    }


    /**
     * Returns a flag that is used to indicate
     * whether a given word has the given inflection
     * or not.
     *
     * @return the 'has' flag
     */
    public boolean has() {
        return hasInflection;
    }


    /**
     * Returns the default singular first.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultSingularFirst(){
        return null;
    }


    /**
     * Returns the default singular second.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultSingularSecond(){
        return null;
    }


    /**
     * Returns the default singular third.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultSingularThird(){
        return null;
    }


    /**
     * Returns the default masculine singular third.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultSingularThirdMasculine() { return null; }


    /**
     * Returns the default feminine singular third.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultSingularThirdFeminine() { return null; }


    /**
     * Returns the default plural first.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultPluralFirst(){
        return null;
    }


    /**
     * Returns the default plural second.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultPluralSecond(){
        return null;
    }


    /**
     * Returns the default plural third.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultPluralThird(){
        return null;
    }


    /**
     * Returns the default impersonal.
     * This method can be give the class the ability to
     * generate inflections.
     *
     * If the method returns null it will not get exported.
     *
     * @return Default null. If overridden a list of inflections
     */
    protected List<String> getDefaultImpersonal() { return null; }


    /**
     * Returns a list of all the versions of the word for this tense.
     *
     * @return a list of all the forms
     */
    public List<String> getVersions(){
        List<String> list = new LinkedList<>();

        list.addAll(addIfNotNull(SINGULAR_FIRST, this::getDefaultSingularFirst));
        list.addAll(addIfNotNull(SINGULAR_SECOND, this::getDefaultSingularSecond));
        list.addAll(addIfNotNull(SINGULAR_THIRD, this::getDefaultSingularThird));
        list.addAll(addIfNotNull(SINGULAR_THIRD_MASCULINE, this::getDefaultSingularThirdMasculine));
        list.addAll(addIfNotNull(SINGULAR_THIRD_FEMININE, this::getDefaultSingularThirdFeminine));

        list.addAll(addIfNotNull(PLURAL_FIRST, this::getDefaultPluralFirst));
        list.addAll(addIfNotNull(PLURAL_SECOND, this::getDefaultPluralSecond));
        list.addAll(addIfNotNull(PLURAL_THIRD, this::getDefaultPluralThird));

        list.addAll(addIfNotNull(IMPERSONAL, this::getDefaultImpersonal));

        return list;
    }


    /**
     * Returns the overridden inflections
     * if they exist for the key.
     * If the key does not exist it will try to fetch the defaults.
     * If the defaults return null it will return an empty list
     *
     * @param key the inflection to fetch
     * @param defaults the default getter
     * @return a list of inflections
     */
    private List<String> addIfNotNull(String key, Supplier<List<String>> defaults){
        if(persons.containsKey(key))
            return persons.get(key);
        var list = defaults.get();
        if(list != null)
            return list;
        return Collections.emptyList();
    }

}
