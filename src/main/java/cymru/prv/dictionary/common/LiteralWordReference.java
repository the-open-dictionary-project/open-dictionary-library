package cymru.prv.dictionary.common;

import cymru.prv.dictionary.common.json.JsonSerializable;
import org.json.JSONObject;


/**
 * Represents a literal string reference.
 * For ID based references see IDWordReference.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see IDWordReference
 */
class LiteralWordReference implements JsonSerializable {

    private final String word;

    LiteralWordReference(String word){
        this.word = word;
    }

    @Override
    public JSONObject toJson() {
        return new JSONObject().put("value", word);
    }

}
