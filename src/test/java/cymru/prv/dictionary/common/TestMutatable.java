package cymru.prv.dictionary.common;

import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TestMutatable {

    static class MutatableNoun extends Word {

        public MutatableNoun(JSONObject obj){
            super(obj, WordType.noun);
        }

        // Returns the word
        @Override
        public JSONObject getMutations() {
            return new JSONObject()
                    .put("radical", getNormalForm())
                    .put("soft", "d" + getNormalForm().substring(1));
        }
    }

    @Test
    public void testMutationsAreExported(){
        MutatableNoun noun = new MutatableNoun(
                new JSONObject().put("normalForm", "test"));
        JSONObject obj = noun.toJson();
        Assertions.assertEquals("test", obj.getJSONObject("mutations").get("radical"));
        Assertions.assertEquals("dest", obj.getJSONObject("mutations").get("soft"));
    }

}
