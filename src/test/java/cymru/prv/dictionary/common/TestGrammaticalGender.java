package cymru.prv.dictionary.common;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Tests for grammatical genders
 *
 * @author Preben Vangberg
 * @since 1.0.3
 */
public class TestGrammaticalGender {

    @Test
    public void testSingleGrammaticalGender(){
        var genders = GrammaticalGender.fromString("m");
        Assertions.assertEquals(1, genders.size());
        Assertions.assertTrue(genders.contains(GrammaticalGender.MASCULINE));
    }

    @Test
    public void testMultipleGrammaticalGender(){
        var genders = GrammaticalGender.fromString("aic");
        Assertions.assertEquals(3, genders.size());
        Assertions.assertTrue(genders.contains(GrammaticalGender.ANIMATE));
        Assertions.assertTrue(genders.contains(GrammaticalGender.INANIMATE));
        Assertions.assertTrue(genders.contains(GrammaticalGender.COMMON));
    }

    @Test
    public void testNoGrammaticalGender(){
        var genders = GrammaticalGender.fromString("");
        Assertions.assertEquals(0, genders.size());
    }

    @Test
    public void testMissingGrammaticalGender(){
        Assertions.assertThrows(IllegalArgumentException.class, () -> GrammaticalGender.fromString("å"));
    }

    @Test
    public void testNullStringShouldReturnNull(){
        var genders = GrammaticalGender.fromString(null);
        Assertions.assertNull(genders);
    }

    @Test
    public void testToStringShouldReturnOneLetterCode(){
        Assertions.assertEquals("f", GrammaticalGender.FEMININE.toString());
    }

}
