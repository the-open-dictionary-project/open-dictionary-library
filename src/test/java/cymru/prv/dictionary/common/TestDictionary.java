package cymru.prv.dictionary.common;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.*;

public class TestDictionary {

    private Word newWord(JSONObject object){
        return new Word(object, WordType.verb);
    }

    private Dictionary getDictionary() {
        return getDictionary("test");
    }

    private Dictionary getDictionary(String name){
        return new Dictionary(name, Map.of(
                WordType.verb, this::newWord
        ));
    }

    @Test
    public void testShouldReturnWordInFile() {
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("testWord");
        Assertions.assertEquals(1, words.size());
        Word word = words.get(0);
        Assertions.assertEquals("testWord", word.getNormalForm());
    }

    @Test
    public void testShouldFetchWordById() {
        Dictionary dictionary = getDictionary();
        Word word = dictionary.getWordById(5);
        Assertions.assertNotNull(word);
        Assertions.assertEquals("wordWithId", word.getNormalForm());
    }

    @Test
    public void testShouldThrowIfIdExist() {
        Assertions.assertThrows(
                RuntimeException.class,
                () -> getDictionary("duplicate")
        );
    }

    @Test
    public void testShouldReturnEmptyListIfWordDoesNotExist() {
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("qwertyuio");
        Assertions.assertEquals(0, words.size());
    }


    class TestVerb extends Word {

        private String stem;

        public TestVerb(JSONObject obj) {
            super(obj, WordType.verb);
            stem = obj.optString("stem", getNormalForm().replaceFirst("io$", ""));
        }

        @Override
        protected JSONObject getConjugations() {
            return new JSONObject().put("past", stem + "ais");
        }
    }

    @Test
    public void testShouldOnlyReturnWordsOfGivenType() {
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("testType", WordType.verb);
        Assertions.assertEquals(1, words.size());
        Assertions.assertEquals("testType", words.get(0).getNormalForm());
        Assertions.assertNull(words.get(0).getConjugations());
    }

    @Test
    public void testShouldReturnWordIfItExistWithType() {
        Dictionary dictionary = new Dictionary("test", Map.of(
                WordType.verb, TestVerb::new
        ));
        List<Word> words = dictionary.getWords("testing", WordType.verb);
        Assertions.assertEquals(1, words.size());
        JSONObject word = words.get(0).toJson();
        Assertions.assertEquals("testais", word.getJSONObject("conjugations").getString("past"));
    }

    @Test
    public void testShouldGenerateWordIfWordDoesNotExist() {
        Dictionary dictionary = new Dictionary("test", Map.of(
                WordType.verb, TestVerb::new
        ));
        List<Word> words = dictionary.getWords("ceisio", WordType.verb);
        Assertions.assertEquals(1, words.size());
        JSONObject word = words.get(0).toJson();
        Assertions.assertFalse(word.getBoolean("confirmed"));
        Assertions.assertEquals("ceisais", word.getJSONObject("conjugations").getString("past"));
    }

    @Test
    public void testShouldThrowIfWordTypeIsNotImplemented() {
        Dictionary dictionary = getDictionary();
        Assertions.assertThrows(
                RuntimeException.class,
                () -> dictionary.getWords("", WordType.adjective)
        );
    }


    @Test
    public void testVersionsWithLiteralAndIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("referenceTest");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("versions");

        JSONObject idRef = array.getJSONObject(0);
        JSONObject litRef = array.getJSONObject(1);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));

        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testRelatedWithLiteralAndIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("relatedTest");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("related");

        JSONObject idRef = array.getJSONObject(0);
        JSONObject litRef = array.getJSONObject(1);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));

        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testSynonymWithLiteralAndIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("synonymTest");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("synonyms");

        JSONObject idRef = array.getJSONObject(0);
        JSONObject litRef = array.getJSONObject(1);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));

        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testAntonymWithLiteralAndIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("antonymTest");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("antonyms");

        JSONObject idRef = array.getJSONObject(0);
        JSONObject litRef = array.getJSONObject(1);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));

        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testTroponymWithLiteralAndIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("troponymTest");
        for(var word : words)
            Assertions.assertTrue(word.isInDictionary());

        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("troponyms");

        JSONObject idRef = array.getJSONObject(0);
        JSONObject litRef = array.getJSONObject(1);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));

        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testRelatedWithString(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("relatedString");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("related");
        JSONObject litRef = array.getJSONObject(0);
        Assertions.assertEquals("literalReference", litRef.getString("value"));
    }

    @Test
    public void testRelatedWithIdReference(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("relatedID");
        Assertions.assertEquals(1, words.size());

        JSONArray array = words.get(0).toJson().getJSONArray("related");

        JSONObject idRef = array.getJSONObject(0);

        Assertions.assertEquals(25, idRef.getLong("id"));
        Assertions.assertEquals("idTest", idRef.getString("value"));
    }

    @Test
    public void testFetchLanguageCode(){
        Dictionary dictionary = getDictionary();
        Assertions.assertEquals("test", dictionary.getLanguageCode());
    }

    @Test
    public void testLiteraryVersionsShouldRedirect(){
        Dictionary dictionary = getDictionary();
        List<Word> words = dictionary.getWords("versionTest");
        Assertions.assertEquals(1, words.size());
        Assertions.assertEquals("testVersions", words.get(0).getNormalForm());
    }

    @Test
    public void testDictionaryWordCount(){
        Dictionary dict = new Dictionary();
        Assertions.assertEquals(0, dict.getNumberOfWords());
        dict.addWord(new Word(new JSONObject().put("normalForm", "hello"), WordType.noun));
        Assertions.assertEquals(1, dict.getNumberOfWords());
    }

    @Test
    public void testGetLemmas(){
        Dictionary dict = new Dictionary();
        dict.addWord(new Word(
                new JSONObject()
                    .put("normalForm", "test")
                    .put("versions", "testversion"),
                WordType.verb
        ));
        var list = dict.getLemmas("testversion");
        Assertions.assertEquals("test", list.get(0).getNormalForm());
    }

    @Test
    public void testIteratorIteratesAllWords(){

        Dictionary dict = new Dictionary();
        Set<String> words = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            words.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());

        // Loop through each word
        for(Word word : dict){
            String nf = word.getNormalForm();
            // Assert that the word is in the list of words
            Assertions.assertTrue(words.contains(nf));
            words.remove(nf);
        }

        // Assert that all the words have been looped through
        Assertions.assertEquals(0, words.size());
    }

    @Test
    public void testIteratorIteratesAllWordsOfType(){

        Dictionary dict = new Dictionary();
        Set<String> verbs = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            if(type == WordType.verb)
                verbs.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());

        // Loop through each verb
        for (Iterator<Word> it = dict.iterateByType(WordType.verb); it.hasNext(); ) {
            Word word = it.next();
            String nf = word.getNormalForm();
            // Assert that the word is in the list of verbs
            Assertions.assertTrue(verbs.contains(nf));
            verbs.remove(nf);
        }

        // Assert that all the verbs have been looped through
        Assertions.assertEquals(0, verbs.size());
    }

    @Test
    public void testGeneratesListOfAllWords(){

        Dictionary dict = new Dictionary();
        Set<String> words = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            words.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());

        // Loop through each word
        for(Word word : dict.getAllWords()){
            String nf = word.getNormalForm();
            // Assert that the word is in the list of words
            Assertions.assertTrue(words.contains(nf));
            words.remove(nf);
        }

        // Assert that all the words have been looped through
        Assertions.assertEquals(0, words.size());
    }

    @Test
    public void testGenerateListByType(){

        Dictionary dict = new Dictionary();
        Set<String> verbs = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            if(type == WordType.verb)
                verbs.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());

        // Loop through each verb
        for (Word word : dict.getAllWordsByType(WordType.verb)) {
            String nf = word.getNormalForm();
            // Assert that the word is in the list of verbs
            Assertions.assertTrue(verbs.contains(nf));
            verbs.remove(nf);
        }

        // Assert that all the verbs have been looped through
        Assertions.assertEquals(0, verbs.size());
    }

    @Test
    public void testCompileToJson(){

        Dictionary dict = new Dictionary();
        Set<String> words = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            words.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());
        var obj = dict.exportDictionaryToJson();
        var wordsArray = obj.getJSONArray("words");

        // Loop through each word
        for (int i = 0; i < wordsArray.length(); i++) {
            var word = wordsArray.getJSONObject(i);
            String nf = word.getString("normalForm");
            // Assert that the word is in the list of verbs
            Assertions.assertTrue(words.contains(nf));
            words.remove(nf);
        }

        // Assert that all the verbs have been looped through
        Assertions.assertEquals(0, words.size());
    }


    public void testCompileToJsonWithEmptyWordTypes(){

        Dictionary dict = new Dictionary();
        Set<String> words = new HashSet<>();
        int numberOfWords = 100;
        Random random = new Random();
        for (int i = 0; i < numberOfWords; i++) {
            WordType type = WordType.values()[random.nextInt(WordType.values().length - 2)];
            String nf = "word" + i + "_" + generateLetters(2);
            dict.addWord(new Word(
                    new JSONObject()
                            .put("normalForm", nf),
                    type
            ));
            words.add(nf);
        }

        // Assert that all words are in the dictionary
        Assertions.assertEquals(numberOfWords, dict.getNumberOfWords());
        var obj = dict.exportDictionaryToJson();
        var wordsArray = obj.getJSONArray("words");

        // Loop through each word
        for (int i = 0; i < wordsArray.length(); i++) {
            var word = wordsArray.getJSONObject(i);
            String nf = word.getString("normalForm");
            // Assert that the word is in the list of verbs
            Assertions.assertTrue(words.contains(nf));
            words.remove(nf);
        }

        // Assert that all the verbs have been looped through
        Assertions.assertEquals(0, words.size());
    }

    
    private String generateLetters(int count){
        Random random = new Random();
        char[] chars = new char[count];
        for (int i = 0; i < count; i++) {
            chars[i] = (char)('a' + random.nextInt(26));
        }
        return new String(chars);
    }

}
