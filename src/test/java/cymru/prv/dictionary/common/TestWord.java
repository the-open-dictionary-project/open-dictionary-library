package cymru.prv.dictionary.common;

import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.function.Function;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestWord {

    @Test
    public void testWordShouldStoreNormalForm(){
        Word word = new Word(new JSONObject().put("normalForm", "test"), WordType.noun);
        JSONObject obj = word.toJson();
        assertEquals("test", obj.get("normalForm"));
    }

    @Test
    public void testWordShouldThrowIfNormalFormDoesNotExist(){
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Word(new JSONObject(), WordType.noun)
        );
    }

    @Test
    public void testWordShouldThrowIfNormalFormIsEmpty(){
        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> new Word(new JSONObject().put("normalForm", "\t"), WordType.noun)
        );
    }

    @Test
    public void testWordShouldReturnEtymologyIfGiven(){
        Word word = new Word(new JSONObject().put("normalForm", "test").put("etymology", "Hello, World!"), WordType.noun);
        JSONObject obj = word.toJson();
        assertEquals("Hello, World!", obj.get("etymology"));
    }

    @Test
    public void testWordShouldNotContainEtymologyIfNotGiven(){
        Word word = new Word(new JSONObject().put("normalForm", "test"), WordType.noun);
        JSONObject obj = word.toJson();
        Assertions.assertFalse(obj.has("etymology"));
    }

    @Test
    public void testWordShouldExportWordType(){
        Word word = new Word(new JSONObject().put("normalForm", "test"), WordType.verb);
        JSONObject obj = word.toJson();
        assertEquals(WordType.verb, obj.getEnum(WordType.class, "type"));
    }

    @Test
    public void testWordShouldExportNoteIfExist(){
        String note = "Hello, World!";
        Word word = new Word(new JSONObject().put("normalForm", "test").put("notes", note), WordType.adjective);
        JSONObject obj = word.toJson();
        assertEquals(note, obj.get("notes"));
    }

    @Test
    public void testWordShouldExportIdIfExist(){
        Word word = new Word(new JSONObject().put("normalForm", "test").put("id", 5), WordType.adjective);
        JSONObject obj = word.toJson();
        assertEquals(5, obj.getLong("id"));
    }

    @Test
    public void testOptionalShouldChooseNonNullValue(){
        JSONObject obj = new JSONObject();
        Word.addOrDefault(obj, "key", "value", () -> "other");
        assertEquals("value", obj.getString("key"));
    }

    @Test
    public void testOptionalShouldChooseOtherIfNull(){
        JSONObject obj = new JSONObject();
        Word.addOrDefault(obj, "key", null, () -> "value");
        assertEquals("value", obj.getString("key"));
    }

    @Test
    public void testWordShouldContainExamplesIfExist(){
        Word word = new Word(
                new JSONObject()
                        .put("normalForm", "test")
                        .put("examples", new JSONArray().put(new JSONObject()
                            .put("text", "myText")
                            .put("trans", "myTranslation")
                            .put("explanation", "myExplanation"))),
                WordType.verb
        );
        JSONObject obj = word.toJson();

        JSONObject example = obj.getJSONArray("examples").getJSONObject(0);
        assertEquals("myText", example.get("text"));
        assertEquals("myTranslation", example.get("trans"));
        assertEquals("myExplanation", example.get("explanation"));
    }

    @Test
    public void testWordShouldReturnTranslation(){
        Word word = new Word(
                new JSONObject()
                    .put("normalForm", "testio")
                    .put("translations", "to test"),
                WordType.verb
        );
        JSONObject obj = word.toJson();
        JSONArray translations = obj.getJSONArray("translations");
        assertEquals(1, translations.length());
        JSONObject unit = translations.getJSONObject(0);
        assertEquals(1, unit.keySet().size());
        JSONArray englishTranslations = unit.getJSONArray("en");
        assertEquals(1, englishTranslations.length());
        assertEquals("to test",
                englishTranslations.getJSONObject(0).getString("value"));
    }

    @Test
    public void testWordShouldReturnConfirmedStatus(){
        Word word = new Word(
                new JSONObject()
                    .put("normalForm", "testio"),
                WordType.verb
        );
        Assertions.assertTrue(word.toJson().getBoolean("confirmed"));
    }

    @Test
    public void generatedWordsShouldReturnLanguageOfDict(){
        var dict = new Dictionary(
                "test",
                Map.of(WordType.verb, w -> new Word(w, WordType.verb))
        );
        var word = dict
                .generateWord(
                        new JSONObject().put("normalForm", "testWord"),
                        WordType.verb)
                .toJson();
        Assertions.assertEquals(dict.getLanguageCode(), word.getString("lang"));

    }
}
